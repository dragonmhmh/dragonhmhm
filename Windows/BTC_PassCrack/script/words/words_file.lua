
require('base')
lstfile = require('lstfile')
assert(lstfile)
assert(type(LST_ROOT)=='string' and LST_ROOT:len()>0)

local words={
	init = nil,
	at = nil,
	eof = nil,
	total = nil,
	hash = nil,
}
function words:init(param)
	self.file = lstfile.open(LST_ROOT..param)
	if not self.file then
		self.file = lstfile.open(LST_ROOT..param..'.lst')
		if not self.file then
			self.file = lstfile.open(LST_ROOT..param..'.txt')
		end
	end
	assert(self.file, "invalid file "..LST_ROOT..param)
end

function words:at(id)
	assert(self.file, "invalid file ")
	return self.file:get(id)
end
function words:total()
	assert(self.file, "invalid file ")
	return self.file:total(), self.file:minlen()
end

function words:hash()
	assert(self.file, "invalid file ")
	return self.file:hash()
end

return words
