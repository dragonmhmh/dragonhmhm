<?php
/*

gen_http.lua -> web -> gen_query.php

"url_next":"http://127.0.0.1/gen_query.php?act=next&id=%id%",
"url_hash":"http://127.0.0.1/gen_query.php?act=hash",
"url_count":"http://127.0.0.1/gen_query.php?act=count"

*/

error_reporting(E_ALL);
ini_set('dispaly_errors','on');

mysql_connect("127.0.0.1", "root", "123456");
mysql_select_db("pass_db");
$table_name = 'password';
/*
	pass_db.password(id bigint,password text)
*/

function db_query($str){
	//echo $str;
	$cursor = mysql_query($str);
	$rs = mysql_fetch_assoc($cursor);
	return $rs;
}


if($_GET['act']=='total'){
	$rs = db_query("select count(*) as total from {$table_name} limit 1");
	if(!$rs['total']){
		$rs = array('total'=>0);
	}
	die( $rs['total'] );
}
else if($_GET['act']=='hash'){
	$rs = db_query("select count(*) as total,min(id) as min_id,max(id) as max_id from {$table_name} limit 1");
	$total = $rs['total'];
	$min_id = $rs['min_id'];
	$max_id = $rs['max_id'];
	$rs = db_query("select * from {$table_name} where id={$min_id} limit 1");
	$pass1 = $rs['password'];
	$rs = db_query("select * from {$table_name} where id={$max_id} limit 1");
	$pass2 = $rs['password'];
	$str = "{$total}:{$min_id}:{$max_id}:{$pass1}:{$pass2}";
	//die($str);
	die(md5($str));
}
else if($_GET['act']=='get'){
	$index = $_GET['index'];
	if(!$index){
		$index = 1;
	}

	$rs = db_query("select * from {$table_name} where id={$index} order by id limit 1");
	if(!$rs['password']){
		die(json_encode(array(
			'finish'=>1,
		)));
	}

	die(json_encode(array(
		'value'=>$rs['password'],
	)));
}
else {
	die('invalid act');
}
