--[[

gen_http.lua -> web -> gen_query.php

"url_get":"http://127.0.0.1/gen_query.php?act=get&index=%index%",
"url_hash":"http://127.0.0.1/gen_query.php?act=hash",
"url_total":"http://127.0.0.1/gen_query.php?act=total"

]]

require('base')
assert(http_get)

local words={
	init = nil,
	at = nil,
	total = nil,
	hash = nil,
}

local function get_url(param, act)
	return param .. '?act=' .. act
end

function words:init(param)
	assert(param)
	self._url_get = get_url(param, 'get')..'&index='
	assert(self._url_get)
	self._url_hash = get_url(param, 'hash')
	self._url_total = get_url(param, 'total')
end

local function safe_http(url)
	assert(type(url)=='string' and url:len()>0, "http request url empty")
	local s = http_get(url)
	assert(type(s)=='string' and s:len()>0, "http response empty. url="..url)
	return s
end
local function ajax(url)
	local s = safe_http(url)
	if type(s)=='string' and s:len()>0 then
		local ret = json_decode(s)
		if type(ret)=='table' and next(ret) then
			return ret
		end
	end
	assert(false, "invalid json value:"..url..'\n'..tostring(s))
end
function words:at(id)
	local url = self._url_get .. id
	local s = ajax(url)
	if type(s)=='table' and type(s.value)=='string' then
		return s.value
	end
	
end
function words:total()
	local s = ajax(self._url_total)
	if type(s)=='table' and s.total then
		return tonumber(s.total), tonumber(s.minlen)
	end
	return 0
end
function words:hash()
	local s = ajax(self._url_hash)
	if type(s)=='table' and s.hash then
		return s.hash
	end
end

return words