local rules={}

table.insert(rules, {
	desc = '英语变形',
	name = 'eng_trans',
    next = function(src, ctx)
        if not ctx.i then
            ctx.i = 0
        end
		if ctx.i >= 6 then
			return
		end
        ctx.i = ctx.i + 1
        if ctx.i == 1 then
            return src .. 's'
        elseif ctx.i == 2 then
            return src .. 'es'
        elseif ctx.i == 3 then
            return src .. src:sub(-1) .. 's'
        elseif ctx.i == 4 then
            if not src:find('[A-Z]') then
                return nil
            end
            return src .. 'S'
        elseif ctx.i == 5 then
            return src .. 'ES'
        elseif ctx.i == 6 then
            return src .. src:sub(-1) .. 'S'
        else 
            assert(false)
        end
	end
});


return {
	desc = '英语变换',
	rules = rules,
}