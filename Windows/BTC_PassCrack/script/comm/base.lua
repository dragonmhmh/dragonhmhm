

function table.clone(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return new_table;
    end
    return _copy(object)
end

function table.deep_equal(a,b)
    local t = type(a)
    if t ~= type(b)  then
        return false
    elseif t=='table' then
        if #a ~= #b then
            return false
        end
        for k, v in pairs(a) do
            if not table.deep_equal(v, b[k]) then
                return false
            end
        end
        for k, v in pairs(b) do
            if v and not a[k] then
                return false
            end
        end
        return true
    elseif t=='userdata' or t=='function' then
        assert(false)
        return a==b
    else
        return a==b
    end
end


function table_sadd(src, val)
	if not val then
		return
	end
	if(type(val)~='table')then
		val = {val}
	end
	local set = {}
	for k,v in pairs(src) do
		set[v] = true
	end
	for k,v in pairs(val) do
		if not set[v] and v then
			set[v] = true
			table.insert(src, v)
		end
	end
end

function string.split(str, delimiter)
	if str==nil or str=='' or delimiter==nil then
		return nil
	end
	
    local result = {}
    for match in (str..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

function has_str_field(opt, field)
	return opt and type(opt[field])=='string' and opt[field]:len()>0
end

function path_basename(path)
    if not path or path:len()<=0 then
        return ''
    end
    local rpath = path:reverse()
    local pos = rpath:find('[/\\]')
    if pos then
        path = path:sub(-pos+1)
        rpath = path:reverse()
    end
    pos = rpath:find('.', nil, true)
    if pos then
        path = path:sub(1, -pos-1)
    end
    return path
end

table.dump = function(t)
	local str = '{'
    for i,v in pairs(t) do
        if i~=1 then
            str = str .. ','
        end
		str = str .. v
    end
    str = str..'}'
	return (str)
end

json_encode = json_encode or function(src)
	local j = json or require('json')
	return j.encode(src)
end
json_decode = json_decode or function(src)
	local j = json or require('json')
	return j.decode(src)
end

function var_dump(src)
    print(json_encode(src))
end

if not math.pow then
    function math.pow(a,b)
        local ret = a^b
        return ret
    end
end