require('base')

assert(SCRIPT_ROOT);
package.path=SCRIPT_ROOT..'words/?.lua;'..package.path;

local MAX_COUNT = 88

local function idlist_next(idlist, maxlen, minval, maxval)
	local i = 1
	while i <= maxlen do
		idlist[i] = (idlist[i] or (minval-1))+1
		if idlist[i] > maxval then
			idlist[i] = minval
			i = i + 1
		else
			return true
		end
	end
	return false
end

------------------------------------------------------------------------------
local gen={}
function gen:init(opt, ctx)
    self.opt = opt
	opt.maxlen = opt.maxlen or MAX_COUNT
    self.mobjs = {}
    assert(opt.module)
    self.implClass = require('words_'..opt.module)
    self.mobjs[1] = {}
    self.implClass.init(self.mobjs[1], self.opt.module_param)
    self.maxcount, self.minlen = self.implClass.total(self.mobjs[1])
    self.maxlen = math.ceil( opt.maxlen / math.max(self.minlen or 1, 1)  )
    assert(self.maxlen>=1)
    
    if not ctx.idlist then
        ctx.idlist = {}
    end
end

function gen:next(ctx)
    if not idlist_next(ctx.idlist, self.maxlen, 1, self.maxcount) then
        return false
    end
    local ret = {}
    local str = ''
    local keys = {}
	for h,id in pairs(ctx.idlist) do
		local obj = self.mobjs[h]
        if not obj then
            obj = {}
            self.mobjs[h] = obj
            self.implClass.init(obj, self.opt.module_param)
        end
        local k = self.implClass.at(obj, id)
        table.insert(keys, k)
        str = str .. k
	end
	return str, keys
end
function gen:hash()
    local data = {
        ver = 1,
        maxlen = self.opt.maxlen,
        module = self.opt.module,
        module_param = self.opt.module_param,

        sub = self.implClass.hash(self.mobjs[1]),
    }
    return md5(json_encode(data))
end


return gen