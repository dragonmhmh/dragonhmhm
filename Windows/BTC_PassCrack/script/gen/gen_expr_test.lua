local gen = require('gen_expr')

------------------------------------------------------------------------------

local function test_gen(opt)
	local ct = os.clock()
	local ctx={}
	local obj={}
	gen.init(obj, opt, ctx)
    local maxtime = 60
    local total = 0
	local start = os.time()
	local timeout = false
	local exist = {}
	while true do
		local str = gen.next2(obj, ctx)
        if not str then
			break
        end
		total = total + 1
		
		if total < 200000 then
			if exist[str] then
				print('exist='..str)
			end
			exist[str] = true
		end

		--print(str)
		
		if os.time() - start > maxtime then
			timeout = true
			local process = total *100 / obj._maxcount
			print('is timeout: ('..process..'%) ' .. total .. '/' .. obj._maxcount)
            break
        end
	end
    ct = os.clock() - ct
    local speed = total / ct
	print("taketime="..ct..",total="..total..", speed="..speed)
	if not timeout then
		if obj._maxcount ~= total then
			print('count diff ' .. total .. '/' .. obj._maxcount)
			assert(false)
		end
	end
end

local function test_gen2(expr)
	test_gen({expr = expr})
end

if TEST_MODE or TEST_TARGET=='expr'  then
	test_gen2("[{raw:BTC|123456}a]{0,1}")

	test_gen2("fuck[abc]{0,5}")
	test_gen2("[{raw:p1|p2|p3}]{0,2}[{raw:1|2|3}]{0,2}[abc]{0,2}")
	test_gen2("[{file:age.lst}bcd{raw:123|345}]{1,2}[{raw:123|345}]{0,3}")
	test_gen2("[{file:age.lst}bcd]{1,2}[z]{0,2}")
	--[[
	test_gen2("fuck[abc]*")
	test_gen2("[{file:password.lst}bcd]+[z]*")
	test_gen2("[{file:tt.txt}bcd]+[{raw:123|345}]*")
	]]
    print('test finish')
end


return gen