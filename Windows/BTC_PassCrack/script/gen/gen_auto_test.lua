local gen = require('gen_auto')


local function test_gen(opt)
	local ct = os.clock()
	local ctx={}
	local obj={}
    gen.init(obj, opt, ctx)
    local maxtime = 60
    local total = 0
    local start = os.time()
	while true do
		local str = gen.next(obj, ctx)
        if not str then
			break
        end
        total = total + 1
        --print(str)
        if os.time() - start > maxtime then
            break
        end
	end
    ct = os.clock() - ct
    local speed = total / ct
	print("taketime="..ct..",total="..total..", speed="..speed)
end

if TEST_MODE or TEST_TARGET=='auto' then
    --[[test_gen( {expr='[a-zA-Z{raw:hehe}]{0,2}', 
        rule_cmd_list='case_all'
    }  )]]

    local opt = {
        --  expr="",
        maxlen = 30,
--        expr='[{file:keys}]+',
        expr='[{raw:aa|bb}]+',
        rule_cmd_list={
--            { "case_all(1)", "case_all(2)", "case_all(5)", "case_all(7)" },
            "case_all",
--            "input_error"
        },
        rule_cmd_list2={
            {"all(1)", "all(2)", "all(5)", "all(7)" },
            --{"all"}
            --"case_all",
            --{"eng_trans"},
        },
        rule_minlen = 2,
    }
    test_gen(opt)

    print('test finish')
end

return gen