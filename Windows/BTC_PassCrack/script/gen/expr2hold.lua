require('base')

--[[

 raw string ((aa)|(bb)|(cc))

]]
local MAX_COUNT = 88
local BREAK_KEY_RAW = { ['\\']=true, ['[']=true }
local BREAK_KEY_COUNT_CLOSE = { ['}']=true }
local BREAK_KEY_SPEC = { ['\\']=true, [']']=true, ['{']=true, ['-']=true }
local BREAK_KEY_FROM_CLOSE = { ['}']=true }
local SPEC_KEYS_COUNT = { ['*']=true, ['+']=true, ['?']=true, ['{']=true }

local function string2charTable(str)
	local list = {}
	for i=1,#str do
		local ch = string.sub(str, i, i)
		table.insert(list, ch)
	end
	return list
end

local STATE = {
	raw = 1,
	escape = 2,
	regex = 3,
	regex_close = 4,
	regex_range = 5,
	from = 6,
	count = 7,
}

local stack={}
function stack.push(s, newval)
	table.insert(s, newval)
	return newval
end
function stack.pop(s)
    local ret = s[#s];
	table.remove(s, #s)
	return ret
end
function stack.skip_eof(s)
	table.remove(s, #s)
	return s[#s]
end
------------------------------------------------------------------------------

local function expr_to_holder(expr)
	local holder = {}
	local str = ''
	local breakStack = {}
	local breakMap = stack.push(breakStack, BREAK_KEY_RAW)
	local stateStack = {}
	local state = stack.push(stateStack, STATE.raw)
	local last = holder[#holder] or {}
	local i = 1
	local strlen = #expr
	while i <= strlen do
		local ch = string.sub(expr, i, i)
		--print(ch,state)
		if breakMap and not breakMap[ch] then
			str = str .. ch
		elseif state==STATE.escape then
			state = stack.skip_eof(stateStack)
			breakMap = stack.skip_eof(breakStack)
			str = str .. ch
		elseif ch == "\\" and state~=STATE.escape then
			state = stack.push(stateStack, STATE.escape)
			breakMap = stack.push(breakStack, {})
			breakMap = nil
		elseif state==STATE.raw then
			if ch == '[' then
				if #str>0 then
					last = { select={} }
					table.insert(last.select, { keys={str} })
					table.insert(holder, last)
					str = ''
				end
				last = { select={} }
				state = stack.push(stateStack, STATE.regex)
				breakMap = stack.push(breakStack, BREAK_KEY_SPEC)
			else
				return nil, assert(false, string.format('invalid char(%d)%s on state=%d', i, ch, state) )
			end
		elseif state==STATE.regex_range then
			local firstChar = string.byte(str:sub(1,1))
			local lastChar = string.byte(ch)
			if firstChar <= lastChar then
				local newstr = ''
				for i=firstChar, lastChar do
					newstr = newstr .. string.char(i)
				end
				table.insert(last.select, { keys=string2charTable(newstr) })
			end
			str = ''
			state = stack.skip_eof(stateStack)
			breakMap = stack.skip_eof(breakStack)
		elseif state==STATE.regex then
			if ch == '-' then
				if #str>=1 then
					local laststr = str:sub(1,-2)
					if #laststr > 0 then
						table.insert(last.select, { keys=string2charTable(laststr) })
					end
					str = str:sub(-1) .. ch
					state = stack.push(stateStack, STATE.regex_range)
					breakMap = stack.push(breakStack, {})
					breakMap = nil
				else
					str = str .. ch
				end
			elseif ch==']' or ch == '{' then
				if #str>0 then
					table.insert(last.select, { keys=string2charTable(str) })
					str = ''
				end
				if ch==']' then
					table.insert(holder, last)
					state = STATE.regex_close
					breakMap = nil
				elseif ch == '{' then
					state = stack.push(stateStack, STATE.from)
					breakMap = stack.push(breakStack, BREAK_KEY_FROM_CLOSE)
				end
			else
				return nil, assert(false, string.format('invalid char(%d)%s on state=%d', i, ch, state) )
			end
		elseif state==STATE.from then
			if ch == '}' then			
				assert( #str > 0 )
				local name, param = string.match(str, '([^:]+)[:]*(.*)')
				if param=='' then
					param = nil
				end
				assert(name)
				table.insert(last.select, { from = name, param=param })				
				str = ''
				state = stack.skip_eof(stateStack)
				breakMap = stack.skip_eof(breakStack)
			else
				assert(false, string.format('invalid char(%d)%s on state=%d', i, ch, state) )
			end
		elseif state==STATE.regex_close then
			state = stack.skip_eof(stateStack)
			breakMap = stack.skip_eof(breakStack)

			if str=='' and SPEC_KEYS_COUNT[ch] then
				if ch=='{' then
					state = stack.push(stateStack, STATE.count)
					breakMap = stack.push(breakStack, BREAK_KEY_COUNT_CLOSE)
				elseif ch == '+' or ch == '*' or ch=='?' then
					if ch == '+' then
						last.count = { min=1 }
					elseif ch == '*' then
						last.count = { min=0 }
					elseif ch == '?' then
						last.count = { min=0, max=1 }
					else
						assert(false, string.format('invalid char(%d)%s on state=%d', i, ch, state) )
					end
				else
					assert(false, string.format('invalid char(%d)%s on state=%d', i, ch, state) )
					return nil
				end
			else
				str = ''
				i = i - 1
			end
		elseif state==STATE.count then
			if ch == '}' then
				local vmin,vmax = string.match(str, "([%d]+),([%d]*)")
				if not vmin then
					vmin = tonumber(str)
					vmax = vmin
				end
				assert(vmin)
				last.count = { min=tonumber(vmin), max=tonumber(vmax) }
				str = ''
				state = stack.skip_eof(stateStack)
				breakMap = stack.skip_eof(breakStack)
			else
				assert(false, string.format('invalid char(%d)%s on state=%d', i, ch, state) )
			end
		else
			assert(false, string.format('invalid char(%d)%s', i, ch) )
		end
		i = i + 1
	end
	if state==STATE.regex_close then
		state = stack.skip_eof(stateStack)
		breakMap = stack.skip_eof(breakStack)
	end
	if #str>0 then
		if state==STATE.raw then
			last = { select={} }
			table.insert(last.select, { keys={str} })
			table.insert(holder, last)
		end
	end
	
	
	if VERBOSE then
		print('####', expr, state)
		--var_dump(breakStack)
		var_dump(holder)
	end
	assert(#breakStack==1, "not close expr "..#breakStack)	
	
	-- 去重  
	for _1,h in pairs(holder) do
		local charList={}
		local exist = {}
		local newlist = {}
		for _2, rs in pairs(h.select) do
			if rs.keys then
				for _3, ch in pairs(rs.keys)do
					if not exist[ch] then
						exist[ch] = true
						table.insert(charList, ch)
					end
				end
			else
				table.insert(newlist, rs)
			end
		end

		if next(charList) then
			table.insert(newlist, 1, {keys=charList})
		end
		h.select = newlist
	end
	
	--  默认count
	for _1,h in pairs(holder) do
		if not h.count then
			h.count = {min=1,max=1}
		end
		if type(h.count)=='number' then
			h.count = {min=h.count, max=h.count}
		end
		if not h.count.max then
			h.count.max = MAX_COUNT
		end
		if not h.count.min then
			assert(false)
		end
	end	

	if VERBOSE then
		print('####', expr)
		var_dump(holder)
	end

	--[[
	
	holder={
		[hold.1] = {
			count={min=xxx,max=xxx},
			select={
				[1]={
					keys = {'a','b','c',...},	-- [option]
					from = 'aaa', 	-- [option]
					param = 'xxx' 	-- [option]
				}, [2]={...}, ...
			}
		},
		[hold.2]={...}, ...
	}
	
	]]
	return holder
end

------------------------------------------------------------------------------

return { expr_to_holder=expr_to_holder }